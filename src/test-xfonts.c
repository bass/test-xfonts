#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>

#define RESOLUTION_DPI "112"

int main() {
	Display *display = XOpenDisplay(NULL);
	if (!display) {
		perror(NULL);
		return 1;
	}

	const int fontNamesLength = 10;
	const char *fontNames[] = {
			"-monotype-courier new-medium-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-m-*-",	// TTF, modern X.org implementations
			"-monotype-courier new-medium-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-c-*-",	// ditto
			"-monotype-courier new-medium-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-p-*-",	// Type1 (converted from TTF with fontforge)
			"-monotype-courier new-semilight-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-m-*-",	// XFree86 4.3+ and X.org 6.7
			"-monotype-courier new-semilight-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-c-*-",	// ditto
			"-monotype-courier new-regular-r---*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-m-*-",		// TTF, Xsun
			"-monotype-courier-regular-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-m-*-",		// TTF, Xsun
			"-monotype-courier-regular-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-p-*-",		// ???, Xsun
			"-monotype-couriernew-medium-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-m-*-",	// TTF, Xsun, available for ISO-8859-8 (Hebrew) only
			"-unknown-courier new-medium-r-normal--*-120-" RESOLUTION_DPI "-" RESOLUTION_DPI "-p-*-",	// TTF, Xsun, user-defined
	};

	const int encodingsLength = 10;
	const char *encodings[] = {
			"iso8859-1",
			"iso8859-2",
			"iso8859-5",
			"iso8859-8",
			"iso8859-9",
			"iso8859-15",
			"koi8-r",
			"microsoft-cp1251",	// XFree86, X.org
			"ansi-1251",		// Xsun
			"iso10646-1",		// not available on Xsun
	};

	for (int i = 0; i < fontNamesLength; i++) {
		for (int j= 0; j < encodingsLength; j++) {
			const char *fontName = fontNames[i];
			const char *encoding = encodings[j];
			const size_t nameLength = strlen(fontName) + strlen(encoding);
			char *name = malloc(nameLength + 1);
			int charsCopied = sprintf(name, "%s%s", fontName, encoding);
			if (nameLength != charsCopied) {
				printf("length: %zu; char(s) copied: %d\n", nameLength, charsCopied);
			}
			XFontStruct *font = XLoadQueryFont(display, name);
			if (font != NULL) {
				printf("%s: ascent: %d; descent: %d\n", name, font->ascent, font->descent);
				const int code = XFreeFont(display, font);
				if (code != 1) {
					printf("XFreeFont(\"%s\") returned %d.\n", name, code);
				}
			}
			free(name);
		}
	}

	int code = XCloseDisplay(display);
	if (code) {
		printf("XCloseDisplay() returned code %d.\n", code);
		perror(NULL);
		return 2;
	}

	return 0;
}
